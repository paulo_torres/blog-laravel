<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Gate;
use App;
use Auth;
//use Kernel;

class SiteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');  
    }

    public function novo(){
    	return view('novopost');
    }

    public function insere(Request $request){
        $this->validate($request, [
        'titulo' => 'required|unique:posts|max:35|min:3',
        'texto' => 'required|max:255|min:3',
        ]);

        $post = \App\Post::create($request->all());
    	return redirect('/');
    }

    public function show($id)
    {
        # code...
        $post = \App\Post::findOrFail($id);

        return view('show',compact('post'));
    }

    public function atualizar($id)
    {
        # code...
        $post = \App\Post::find($id);
        $this->authorize('modificar',$post);

        return view('atualizar',compact('post'));
    }

    public function gravaatualizacao(Request $request, $id){

        $post = \App\Post::find($id);
        $this->authorize('modificar',$post);

        $post->titulo = $request->input('titulo');
        $post->texto = $request->input('texto');
        $post->save();

        return redirect('/');
    }

    public function apagapost($id){
        $post = \App\Post::find($id);
        $this->authorize('apagar',$post);
        $post->delete(); 
               
        return redirect('/');
    }
}
