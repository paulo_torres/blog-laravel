<?php

namespace App\Http\Controllers;

use Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    public function listaPosts(){

        $dados = \App\Post::all();
        //$user = \Auth::user();


        //\Auth::loginUsingId(1);
        //\Auth::logout();

        /*if(Gate::denies('modificar', $dados)){
                \App::abort(403,'Não Acessivel');
        }*/

        //$this->authorize('modificar',$dados);
        
        return view('listarPosts', compact('dados'));
    }
}
