<?php



//Route::get('/', ['middleware' => 'auth','uses' => 'HomeController@listaPosts']);
//Route::get('/home', ['middleware' => 'auth','uses' => 'HomeController@listaPosts']);


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@listaPosts');
    //Route::get('/wel','TesteController@index');
    Route::get('/teste',function(){
    	return view('teste');
    });
	Route::get('/post/novo','SiteController@novo');
	Route::post('/post/insere','SiteController@insere');
//Route::get('/home','SiteController@listaPosts');
	Route::get('/post/{id}','SiteController@show');
	Route::get('/post/{id}/atualizar','SiteController@atualizar');
	Route::post('/post/{id}/atualizar','SiteController@gravaatualizacao');
	Route::get('/post/{id}/apagar','SiteController@apagapost');


    Route::get('/', 'HomeController@listaPosts');
});
