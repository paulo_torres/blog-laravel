<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }


        .btn.outline {
            background: none;
            padding: 12px 22px;
        }
            .btn-primary.outline {
            border: 2px solid #0099cc;
            color: #0099cc;
        }
        .btn-primary.outline:hover, .btn-primary.outline:focus, .btn-primary.outline:active, .btn-primary.outline.active, .open > .dropdown-toggle.btn-primary {
            color: #33a6cc;
            border-color: #33a6cc;
        }
        .btn-primary.outline:active, .btn-primary.outline.active {
            border-color: #007299;
            color: #007299;
            box-shadow: none;
        }

    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Blog</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li role="presentation" class="active">
                    <a href="/"> 
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span> 
                        Listar
                    </a>
                </li>
                <li role="presentation">
                    <a href="/post/novo">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                        Novo Post
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}"><label>Login</label></a></li>
                    <li><a href="{{ url('/register') }}">Cadastre-se</a></li>
                @else
                    <li class="dropdown" class="active">
                        <a href="#" class="btn btn-success outline dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li> 

                @endif
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    @yield('content')

</body>
</html>
