@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                {{ Form:: open(array('action' => 'SiteController@insere')) }} 
                {{ Form::label('username', 'Nome')}}
                {{ Form::text('username') }}

                {{ Form::select('animal', array(
                    'Cats' => array('leopard' => 'Leopard'),
                    'Dogs' => array('spaniel' => 'Spaniel'),
                )) }}
                {{ link_to('home', $title = "HOME", $attributes = array(), $secure = null) }}
                {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}

                {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
