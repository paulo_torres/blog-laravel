@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Editar Posts</title>

    <!-- Bootstrap -->
    <link href="../../css/app.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
         
        <div class="container">
            <div class="header clearfix">
                <nav>
                  <ul class="nav nav-pills pull-right">
                    <li role="presentation" ><a href="/">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>Listar</a>
                    </li>
                    <li role="presentation" class="active"><a href="/post/novo">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Novo Post</a>
                    </li>
                  </ul>
                </nav>
                <h3 class="text-muted">Atualização de Postagem</h3>
            </div>

            <div class="jumbotron">
                <form method='post' action='/post/{{$post->id}}/atualizar'>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <label for='titulo'>Título</label><br>
                            <input type='text' name='titulo' id='titulo' value="{{$post->titulo}}" class="form-control">
                        </div>
                        <div class="panel-body">
                            <label for='texto'>Texto</label><br>
                            <textarea name='texto' id='texto' col='50' rows='4' class="form-control">
                                {{$post->texto}}
                            </textarea>
                        </div>
                        <div class="panel-footer">
                            <input type='hidden' name='publicado' id='publicado' value="{{$post->publicado}}"><p>
                            {!! csrf_field() !!}
                            <input type='submit' name='cmd_atualizar' value='Atualizar Post' class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>

            <footer class="footer">
                <p>&copy; 2016 Company, Inc.</p>
            </footer>
        </div> <!-- /container -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../js/bootstrap.min.js"></script>
  </body>
</html>
@endsection