@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BLOG LARAVEL</title>

    <!-- Bootstrap -->
    <link href="../css/app.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  	<div class="container">

      <div class="header clearfix">
        <h3 class="text-muted">Lista de Postagens</h3>
      </div>

      <div class="jumbotron">
        <div class="row">
            @if($dados)
        			@foreach($dados as $post)
              <div class="col-sm-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="/post/{{$post->id}}">{{$post->titulo}}</a>
                            <span class="badge navbar-right">{{$post->id}}</span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        {{$post->texto}}
                    </div>
                    @can('modificar',$post)
                            <div class="panel-footer">
                              <a class="btn btn-xs btn-warning" role="button" href="/post/{{$post->id}}/atualizar">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Editar</a>
                              <a class="btn btn-xs btn-danger" role="button" href="/post/{{$post->id}}/apagar">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Apagar</a>
                            </div>
                    @endcan
                    @cannot('modificar',$post)
                        <div class="alert alert-warning" role="alert">Você não possui permissão para editar esse POst.</div>
                    @endcan
                </div>
              </div>
        			@endforeach
            @else
              <p>Sem Postagem</p>
    			  @endif
        </div>
      </div>
      <footer class="footer">
        <p>&copy; 2016 Company, Inc.</p>
      </footer>
    </div> <!-- /container -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>
@endsection
