@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../css/app.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
         
        <div class="container">
            <div class="header clearfix">
                <h3 class="text-muted">Nova Postagem</h3>
            </div>

            <div class="jumbotron">
                    <form method='post' action='/post/insere' class="form-group">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <label for='titulo'>Título</label>
                                <input type='text' name='titulo' id='titulo' class="form-control">
                                @if ($errors->has('titulo'))
                                    <span class="help-block">
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('titulo') }}</div>
                                    </span>
                                @endif
                            </div>
                            <div class="panel-body">
                                <label for='texto'>Texto</label><br>
                                <textarea name='texto' id='texto' col='30' rows='10' class="form-control">
                                </textarea>
                                @if ($errors->has('texto'))
                                    <span class="help-block">
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('texto') }}</div>
                                    </span>
                                @endif
                            </div>
                            <div class="panel-footer">
                                <input type='hidden' name='publicado' id='publicado' value='1'>
                                <input type='hidden' name='id_usuario' id='id_usuario' value={{ Auth::user()->id }}>
                                {!! csrf_field() !!}
                                <input type='submit' name='cmd_postar' value='Postar' class="btn btn-lg btn-success">
                            </div>
                        </div>
                    </form>
            </div>  

            <footer class="footer">
                <p>&copy; 2016 Company, Inc.</p>
            </footer>
        </div> <!-- /container -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>
@endsection